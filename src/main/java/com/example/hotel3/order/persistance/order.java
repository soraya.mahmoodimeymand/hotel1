package com.example.hotel3.order.persistance;


import com.example.hotel3.base.RunConfiguration;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "orders",schema = RunConfiguration.DB)

public class order  {
    @Id
    @GeneratedValue(strategy = GeerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private  boolean orderStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private user user;

    @OneToOne(mappedBy = "order",fetch = FetchType.LAZY)
    private pardakht pardakht;



}
