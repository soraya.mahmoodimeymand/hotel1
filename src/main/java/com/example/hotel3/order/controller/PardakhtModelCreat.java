package com.example.hotel3.order.controller;

import com.example.hotel3.user.controller.NotBlank;

public class PardakhtModelCreat {
    @NotBlank(message = "شماره کارت خود را وارد کنید")
    private String shomarecart;

    @NotBlank(message = "رمز خود را وارد کنید")
    private String ramzcart;

    @NotBlank(message = "مبلغ را وارد کنید و وارد درگاه پرداخت شوید")
    private String costcomingpardakht;

}
