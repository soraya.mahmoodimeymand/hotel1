package com.example.hotel3.admin.service;


import com.example.hotel3.category.persistance.category;
import com.example.hotel3.category.service.categoryService;
import com.example.hotel3.hotel.controller.HotelCreatModel;
import com.example.hotel3.hotel.controller.HotelModel;
import com.example.hotel3.hotel.controller.HotelModelUpdate;
import com.example.hotel3.hotel.persistance.AdminVerified;
import com.example.hotel3.hotel.persistance.HotelAdminVerifiedEnum;
import com.example.hotel3.hotel.persistance.hotel;
import com.example.hotel3.hotel.persistance.hotelRepository;
import com.example.hotel3.hotel.service.hotelServicee;
import com.example.hotel3.user.service.userServicee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

import static javax.swing.UIManager.get;

@Service
@Qualifier("AdminService")

public class AdminService {

    private hotelRepository hotelRepository;
    private userServicee userService;
    private com.example.hotel3.category.service.categoryService categoryService;

    @Autowired
    public AdminService(hotelRepository photoRepository, userServicee userService, categoryService categoryService) {
        this.hotelRepository = photoRepository;
        this.userService = userService;
        this.categoryService = categoryService;


    }

    @Override
    public List<hotelModel> readByAdmin() {

        return hotelRepository.findAllByUserIdOOrderByIdDesc(userService.getAuthUser().getId())
                .stream().map(hotelServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void categoryAdminVerfie(Integer id, HotelAdminVerifiedEnum status) {
        hotel hotel = (id);
        hotel.equals(id);
        hotelRepository.save(hotel);
    }

    @Override
    public List<HotelModel> indexByAdmin() {
        return hotelRepository.findByOrderByIdDesc()
                .stream().map(hotelServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public HotelModel showByAdmin(Integer id) {

        return convertEntityToModel(get(id)).toLIST();
    }


    @Override
    public HotelModel create(HotelCreatModel photoModelCreate) throws Exception {
        hotel newhotel = new Hotel();
        newhotel.setname(photoModelCreate.getname());
        newhotel.setPrice(photoModelCreate.getPrice());
        Set<category> categories = categoryService.getcategories(photoModelCreate.getCategory_id());
        checkCategoriesId(photoModelCreate.getCategories_id(), categories);
        newhotel.setCategory(categories);
        hotelRepository.save(newhotel);
        return convertEntityToModel(newhotel);
    }


    @Override
    public PhotoModel update(PhotoModelUpdate photoModelUpdate) throws Exception {
        hotel hotel = hotel(HotelModelUpdate.getId());
        hotel.setname(HotelModelUpdate.getname());
        Set<Category> category = categoryService.getcategories(photoModelUpdate.getcategory_id());
        photo.setCategory(categories);
        hotelRepository.save(photo);
        return convertEntityToModel(hotel);
    }

    @Override
    public void delete(Integer id) {
        hotel hotel = gethotel(id);
        hotelRepository.delete(hotel);
    }



}
