package com.example.hotel3.category.persistance;

import com.example.hotel3.base.RunConfiguration;
import com.example.hotel3.hotel.persistance.hotel;
import jdk.jfr.Category;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Data
@AllArgsConstructor
@Table( name= "category" , schema = RunConfiguration.DB)
public class category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id")
    private Integer id;


    @Column(name = "Rezarv_price")
    private Integer RezarvPrice ;

    @Column(name = "Cancel_price")
    private Integer RezarvPrice ;


    @Column(name = "name",nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id",referencedColumnName = "id")
    private category parent;

    @OneToMany(mappedBy = "parent",fetch =FetchType.LAZY)
    private Collection<Category> children;

    @ManyToMany(mappedBy = "categories",fetch = FetchType.LAZY)
    private Collection<hotel> photos;



}
