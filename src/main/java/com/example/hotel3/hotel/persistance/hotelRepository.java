
 package com.example.hotel3.hotel.persistance.;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface hotelRepository  extends JpaRepository< hotel,Integer> {
    @Query("SELECT hotel from hotel hotel where hotel.user.id=:id order by hotel.id desc")
    Collection<hotel>findAllByUserIdOOrderByIdDesc(int id);


   Collection<hotel>findByOrderByIdDesc();



}
