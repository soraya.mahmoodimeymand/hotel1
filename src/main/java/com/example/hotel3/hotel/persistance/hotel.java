package com.example.hotel3.hotel.persistance;


import com.example.hotel3.base.RunConfiguration;
import com.example.hotel3.category.persistance.category;
import lombok.Data;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;

import javax.persistence.*;
import java.util.Collection;

@Entity
    @Data
    @Table(name = "photos",schema = RunConfiguration.DB)
    public class hotel {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Integer id;

        @Column(name = "name",nullable = false)
        private String Name;



    @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id",referencedColumnName = "id")
        private user user;


        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(
                name = "categories_hotel",
                joinColumns = @JoinColumn(name = "hotel_id",referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "category_id",referencedColumnName = "id")
        )
        private Collection<category> categories;

        @OneToMany(mappedBy = "hotel",fetch = FetchType.LAZY)
    private Collection<order> orderDetail



    }

}
