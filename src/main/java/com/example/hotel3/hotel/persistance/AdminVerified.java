package com.example.hotel3.hotel.persistance;

import com.example.hotel3.base.RunConfiguration;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

public class AdminVerified {
    @Entity
    @Data
    @Table(name = " Admin_verified", schema = RunConfiguration.DB)
    public class hotel {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Integer id;

        @Column(name = "typ_hotel", nullable = false)
        private String typeHotel;
        @OneToOne
        @OneToMany(mappedBy = "Admin_verifed", fetch = FetchType.LAZY)
        private Collection<AdminVerified> AdminVerifierds;


        @OneToMany(mappedBy = "Admin_verifed", fetch = FetchType.LAZY)
        private Collection<hotel> hotels;

    }
}
