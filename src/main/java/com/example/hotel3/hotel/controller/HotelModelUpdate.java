package com.example.hotel3.hotel.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class HotelModelUpdate {

        private Integer id;

        private String name;

        private Integer price;

        private List<Integer> categories_id;
    }

}
