package com.example.hotel3.hotel.controller;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

public class HotelModel {
    @Data
    public class PhotoModel {
        private Integer id;
        private String name;
        private Integer price;
        private List< HotelCategoryModel> categories = new ArrayList<>();


        private Integer user_id;
        private String username;

    }

}
