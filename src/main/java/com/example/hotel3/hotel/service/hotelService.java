package com.example.hotel3.hotel.service;

import com.example.hotel3.hotel.persistance.HotelAdminVerifiedEnum;

public interface hotelService {

    hotelModel create(hotelModelCreate photoModelCreate) throws Exception;

    hotelModel showByAuthUser(Integer photoModelCreate) throws Exception;

    hotelModel update(hotelModelUpdate photoModelUpdate) throws Exception;

    void delete(Integer id);

    void checkPhotoIsOwn(Integer id);

    List<hotelModel> indexByPhotographer();

    void changeAdminVerified(Integer id, HotelAdminVerifiedEnum status);

    List<hoteloModel> indexByAdmin();

    hotelModel showByAdmin(Integer id);

    List<hotelModel> boughtPhotos();
}


