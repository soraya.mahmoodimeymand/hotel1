package com.example.hotel3.user.controller;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class UserModelUpdate {
    @NotNull
    private Integer id;

    @NotBlank(message = "لطفا رمز عبور را وارد نمایید")
    private String password;


    @NotBlank(message = "لطفا ایمیل را وارد نمایید")
    private String email;

}
