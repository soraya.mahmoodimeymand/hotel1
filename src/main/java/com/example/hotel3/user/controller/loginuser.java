package com.example.hotel3.user.controller;

import lombok.Data;
import java.io.Serializable;

@Data
public class loginuser implements Serializable  {


    private static final Integer serialVersionUID = 3000000;

    @NotBlank(message = "وارد کردن نام کاربری مورد نظر")
    private String username;

    @NotBlank(message = "پسورد مورد نظر خود را وارد کنید")
    private String password;

}
