package com.example.gallery.user.persistence;

import com.example.hotel3.user.persistance.user;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;
import org.springframework.stereotype.Repository;

import javax.management.relation.Role;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository< CustomSQLErrorCodesTranslation, Integer> {
    @Query("select user from user user where user.id=:id")
    List<user> findAllRoleNameByUserId(Integer id);

    user findByUsername(String username);



}
