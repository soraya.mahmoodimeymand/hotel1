package com.example.hotel3.user.persistance;


import com.example.hotel3.base.RunConfiguration;
import com.example.hotel3.hotel.persistance.hotel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "users",schema = RunConfiguration.DB)

public class user {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username",unique = true,nullable = false)
    private String username;

    @Column(name = "password",nullable = false)
    @JsonIgnore
    private String password;


    @Column(name = "is_rezerv")
    private boolean isRezerv;


    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Collection<hotel> photos;


    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Collection<order> orders;


}





