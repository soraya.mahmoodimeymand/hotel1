package com.example.hotel3.user.service;

public class userServicee {
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserServicee(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<UserModel> index() {
        return userRepository.findAll()
                .stream()
                .map(UserServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }

    public UserModel create(UserModelCreate user, String role) {
        Role role1 = roleRepository.findRoleByName(role);
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(role1);
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setActive(true);
        newUser.setRoles(roles);
        userRepository.save(newUser);
        return convertEntityToModel(newUser);
    }

    }

    @Override
    public UserModel show(Integer id) {
        return convertEntityToModel(getUser(id));
    }

    @Override
    public UserModel update(UserModelUpdate userModelUpdate) {
        User user = getUser(userModelUpdate.getId());
        user.setPassword(userModelUpdate.getPassword()));
        userRepository.save(user);
        return convertEntityToModel(user);
    }

    @Override
    public void delete(Integer id) {
        User user = getUser(id);
        userRepository.delete(user);
    }


    public static UserModel convertEntityToModel(User user) {
        UserModel userModel =  new UserModel();
        userModel.setId(user.getId());
        userModel.setUsername(user.getUsername());
        userModel.setPassword(user.getPassword());
        userModel.setEmail(user.getEmail());
        if (user.getRoles()!=null){
            userModel.setRoles(user.getRoles().stream().map(UserServiceImpl::convertRoleEntityToUserRoleModel).collect(Collectors.toList()));
        }

        return userModel;
    }
    public static UserRoleModel convertRoleEntityToUserRoleModel(Role role) {
        UserRoleModel userRoleModel =  new UserRoleModel();
        userRoleModel.setName(role.getName());
        return userRoleModel;
    }

}


}
