package com.example.hotel3.user.service;

import com.example.hotel3.user.controller.UserModel;
import com.example.hotel3.user.controller.UserModelUpdate;
import com.example.hotel3.user.controller.userModelCreat;
import com.example.hotel3.user.controller.userModelCreat;

import java.util.List;


public interface UserService {
   UserModel create(userModelCreat userModelCreate, String role);

    void authenticate(String username, String password) throws Exception;

    UserModel update(UserModelUpdate userModelUpdate);

    UserModel show(Integer id);

    void delete(Integer id);

    List<UserModel> index();

    UserModel getAuthUser();

    void checkUserIsOwn(Integer id);

}
